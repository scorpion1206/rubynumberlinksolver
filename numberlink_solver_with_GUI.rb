#! /home/gemini/.shoes/federales/shoes

Shoes.setup do  
  gem 'ruby-minisat'
end

require "minisat"
$filled = true


def error(msg)
  $stderr.puts msg
  exit 1
end


def parse_file(file)
  width = nil
  field = []
  File.read(file).split(/\n/).each do |line|
    case line
    when /^\s*#.*$/, /^\s*$/
    else
      line = line.split.map {|x| x[/^\d+$/] && x.to_i }
      width ||= line.size
      unless width == line.size
        error "illegal width: row #{ field.size + 1 }"
      end
      field << line
    end
  end

  h = {}
  field.flatten.compact.sort.each_slice(2) do |x, y|
    error "bad field" if x == 0 || x != y || h[x]
    h[x] = true
  end

  field  
end


def define_sat(solver, field)
  w = field.first.size     #number of columns
  h = field.size           #number of rows

  num = field.flatten.compact.uniq.size

  # puts "num = #{num}"

  # define variables:
  #  - 4 variables (directions) to each number cell
  #  - 5 (patterns) + n (numbers) variables to each blank cell
  vars = field.each_with_index.map do |line,row|
    line.each_with_index.map do |c,column|
      # puts "pos:#{row},#{column}"
      if c
        # number cell
        dirs = (0...4).map { solver.new_var }

        # exact one directions
        exact_one(solver, dirs)

        dirs
      else
        # blank cell
        pats = (0...5).map { solver.new_var }
        nums = (0...num).map { solver.new_var }
        # puts "*** nums = #{nums} pats_size = #{pats.length}"

        # no other pattern if blank cell
        pats_without_blank = pats - [pats.last]        
        pats_without_blank.each { |v| solver << ([-pats.last] + [-v]) }  

        # exact two pattern if not blank cell
        maybe_two(solver, pats)

        # zero connected numbers in each blank cell if the cell is blank
        # exact one connected numbers if each blank cell if the cell has a line
        ([pats.last] + nums).combination(2) {|v1, v2| solver << [-v1, -v2] }

        # no blank pattern (if filled solution is required)
        solver << -pats.last if $filled

        [pats, nums]
      end
    end
  end

  # define connection rule
  field.each_with_index do |line, y|
    line.each_with_index do |num, x|
      # puts "## connection_rule : #{y}, #{x} =  #{num}"
      if num
        number_connectivity(solver, field, vars, x, y, w, h)
      else
        line_connectivity(solver, field, vars, x, y, w, h)
      end
    end
  end

    # corner optimization
  l_corner, r_corner = {}, {}
  field.each_with_index do |line, y|
    line.each_with_index do |num, x|
      if num
        l_corner[[x - 1, y + 1]] = r_corner[[x + 1, y + 1]] = true
      else
        if l_corner[[x, y]]
          l_corner[[x - 1, y + 1]] = true
          if !field[y - 1][x + 1]
            solver << [-vars[y][x].first[0], -vars[y][x].first[3], vars[y - 1][x + 1].first[0], vars[y - 1][x + 1].first[4]] << [-vars[y][x].first[0], -vars[y][x].first[3], vars[y - 1][x + 1].first[3], vars[y - 1][x + 1].first[4]]
          end
        else
          solver << [-vars[y][x].first[0], -vars[y][x].first[3]]
        end
        if r_corner[[x, y]]
          r_corner[[x + 1, y + 1]] = true
          if !field[y - 1][x - 1]
            solver << [-vars[y][x].first[0], -vars[y][x].first[2], vars[y - 1][x - 1].first[0], vars[y - 1][x - 1].first[4]] << [-vars[y][x].first[0], -vars[y][x].first[2], vars[y - 1][x - 1].first[2], vars[y - 1][x - 1].first[4]]
          end
        else
          solver << [-vars[y][x].first[0], -vars[y][x].first[2]]
        end
      end
    end
  end

  vars
end


# basic connectivity
    U_CON     =       [1]
L_CON,  R_CON = [3]    ,    [2]
    D_CON     =       [0]


DIR = [[0, -1], [0, 1], [-1, 0], [1, 0]]


def line_connectivity(solver, field, vars, x, y, w, h)
  if x < w - 1 && !field[y][x + 1]
    l_pats, l_nums = vars[y][x]
    r_pats, r_nums = vars[y][x + 1]

    # extended horizontal connectivity
    solver << [ -l_pats[3], r_pats[2] ]    
    solver << [ -r_pats[2], l_pats[3] ]   

    # U-removal rule
    solver << [ -l_pats[0], -l_pats[3], -r_pats[0], -r_pats[2] ]  
    solver << [ -l_pats[1], -l_pats[3], -r_pats[1], -r_pats[2] ]

    # the same number if connected
    l_nums.zip(r_nums) do |l_num, r_num|
      L_CON.each do |i|
        solver << [-l_pats[i], -l_num, r_num] << [-l_pats[i], l_num, -r_num]
      end
    end
  end

  if y < h - 1 && !field[y + 1][x]
    u_pats, u_nums = vars[y][x]
    d_pats, d_nums = vars[y + 1][x]

    # extended vartical connectivity
    solver << [ -u_pats[1], d_pats[0] ]
    solver << [ -d_pats[0], u_pats[1] ]

    # U-removal rule        
    solver << [ -u_pats[2], -u_pats[1], -d_pats[2], -d_pats[0] ]  
    solver << [ -u_pats[3], -u_pats[1], -d_pats[3], -d_pats[0] ]

    # the same number if connected
    u_nums.zip(d_nums) do |u_num, d_num|
      U_CON.each do |i|
        solver << [-u_pats[i], -u_num, d_num] << [-u_pats[i], u_num, -d_num]
      end
    end
  end

  # edges of field
  R_CON.map {|i| solver << -vars[y][x].first[i] } if x == 0
  L_CON.map {|i| solver << -vars[y][x].first[i] } if x == w - 1
  D_CON.map {|i| solver << -vars[y][x].first[i] } if y == 0
  U_CON.map {|i| solver << -vars[y][x].first[i] } if y == h - 1
end


def number_connectivity(solver, field, vars, x, y, w, h)
  rev = [U_CON, D_CON, L_CON, R_CON].zip(DIR).to_a
  num = field[y][x]

  # puts "--#{y},#{x} num = #{num}"
  4.times do |i|
    dir = vars[y][x][i]
    con, (xo, yo) = rev[0]
    x2, y2 = x + xo, y + yo
    if x2 >= 0 && x2 < w && y2 >= 0 && y2 < h && !field[y2][x2]
      # puts "-- --- #{y2},#{x2} dir = #{i} num = #{num-1}"
      pats, nums = vars[y2][x2]      
      solver << [-dir, nums[num - 1]]                # if line goes this way, current cell connect to current num    

      solver << [-dir] + con.map {|i| pats[i] }      # if line goes this way, at least suitable pattern for that direction is true
      solver << [-dir] + [-pats.last]                   # if line goes this way, the cell is not blank

      (1..3).each do |j|
        con, (xo, yo) = rev[j]
        x3, y3 = x + xo, y + yo
        if x3 >= 0 && x3 < w && y3 >= 0 && y3 < h && !field[y3][x3]
          pats, nums = vars[y3][x3]
          con.each do |k| 
            solver << [-dir, -pats[k]]                     # if line goes this way,, other patterns in this direction is false
            # puts "-- --- *** #{y3},#{x3} dir = #{i} sub_dir = #{k} num = #{num-1}"
          end                                              
          solver << [-dir] + (pats - con.map {|k| pats[k] })  # but other patterns is ok, including blank
        end
      end
    elsif x2 >= 0 && x2 < w && y2 >= 0 && y2 < h && num == field[y2][x2]
      # solver << dir
      (1..3).each do |j|
        con, (xo, yo) = rev[j]
        x3, y3 = x + xo, y + yo
        if x3 >= 0 && x3 < w && y3 >= 0 && y3 < h && !field[y3][x3]
          pats, nums = vars[y3][x3]
          con.each do |k| 
            solver << [-dir, -pats[k]]                     # if line goes this way,, other patterns in this direction is false
            # puts "-- --- *** #{y3},#{x3} dir = #{i} sub_dir = #{k} num = #{num-1}"
          end                                              
          solver << [-dir] + (pats - con.map {|k| pats[k] })  # but other patterns is ok, including blank
        end
      end
    else
      solver << -dir
    end
    rev << rev.shift
  end
end


def exact_one(solver, vars)
  solver << vars
  vars.combination(2) do |v1, v2| 
    solver << [-v1, -v2] 
  end
end

def maybe_two(solver, vars)
  pattern_vars = vars - [vars.last]                # all patterns except for blank
  at_least_one = pattern_vars + [vars.last]        # if not blank cell, then at least one pattern
  solver << at_least_one 
  pattern_vars.each do |v| 
    a = ([-v] + vars - [v]) # if contain 1 pattern, then contain at least 1 more pattern
    solver << a
  end  
  pattern_vars.combination(3) {|v1, v2, v3| solver << [-v1, -v2, -v3]}
end

def make_solution(solver, vars, field)
  field.zip(vars).map do |fline, vline|
    fline.zip(vline).map do |num, vs|
      if num
        (0...4).find {|i| solver[vs[i]] }
      else
        pattern_solution = []
        p1 = (0...5).find { |i| solver[vs.first[i]] }
        pattern_solution.push(p1)
        if p1 != 4 #not empty cell
          p2 = (0...5).find { |i| solver[vs.first[i]] && i != p1 }
          pattern_solution.push(p2)
        end 
        pattern_solution       
      end
    end
  end
end

def add_constraint(solver, vars)
  solver << vars.flatten.map {|v| solver[v] ? -v : v }
end

def output_field(solution, field)
  @start_x = 50
  @start_y = 100
  @cell_width = 40
  @cell_height = 40
  @icon_dir = "/home/gemini/Projects/RubyNumberlinkSolver/icons/"

  w = field.first.size
  h = field.size
  ary = (0 ... h).map { (0 ... w).map { nil } }

  solution.each_with_index do |line, y|
    line.each_with_index do |c, x|
      num = field[y][x]
      ary[y][x] = if num           
        stack width: 40, height: 40, left: @start_x + x * @cell_width, top: @start_y + y * @cell_height do         
          case c 
            when 0 then @display_num_bg = image @icon_dir + "Up.png"
            when 1 then @display_num_bg = image @icon_dir + "Down.png"
            when 2 then @display_num_bg = image @icon_dir + "Left.png"
            when 3 then @display_num_bg = image @icon_dir + "Right.png"
          end
          # @display_num_bg = image @icon_dir + "Blank.png"
        end
        stack width: 40, height: 40, left: @start_x + x * @cell_width, top: @start_y + y * @cell_height do
         @display_num = para num ,:align=>'center'
        end 
      else        
        if c.length == 1
          stack(width: 40, left: @start_x + x * @cell_width, top: @start_y + y * @cell_height) { image @icon_dir + "Blank.png" }
        elsif c.length == 2 
          c = c.sort
          if c.first == 0 && c.last == 1 
            stack(width: 40, left: @start_x + x * @cell_width, top: @start_y + y * @cell_height) { image @icon_dir + "UpDown.png" }
          elsif c.first == 0 && c.last == 2
            stack(width: 40, left: @start_x + x * @cell_width, top: @start_y + y * @cell_height) { image @icon_dir + "UpLeft.png" }
          elsif c.first == 0 && c.last == 3
            stack(width: 40, left: @start_x + x * @cell_width, top: @start_y + y * @cell_height) { image @icon_dir + "UpRight.png" }
          elsif c.first == 1 && c.last == 2
            stack(width: 40, left: @start_x + x * @cell_width, top: @start_y + y * @cell_height) { image @icon_dir + "DownLeft.png" }
          elsif c.first == 1 && c.last == 3
            stack(width: 40, left: @start_x + x * @cell_width, top: @start_y + y * @cell_height) { image @icon_dir + "DownRight.png" } 
          elsif c.first == 2 && c.last == 3
            stack(width: 40, left: @start_x + x * @cell_width, top: @start_y + y * @cell_height) { image @icon_dir + "LeftRight.png" } 
          end                    
        end 
      end       
    end
  end
end

def find_loops(solution, field)
  solution = solution.map {|line| line.dup }
  
  #debug
  # solution.each_with_index do |line, y|
  #   line.each_with_index do |num, x|
  #     puts "##solution       at #{y},#{x} c = #{num}"
  #   end
  # end

  tmp_direction = nil
  field.each_with_index do |line, y|
    line.each_with_index do |num, x|      
      next unless num
      # puts "##find_loops at #{y},#{x} value = #{num} direction = #{solution[y][x]}"
      xo, yo = DIR[solution[y][x]]
      tmp_direction = solution[y][x]
      solution[y][x] = nil
      find_loops_aux(solution, field, tmp_direction, x + xo, y + yo)
    end
  end

  loops = []
  # puts "##Remaining loops"  
  solution.each_with_index do |line, y|
    line.each_with_index do |c, x|
      # puts "##       at #{y},#{x} c = #{c}"
      next if !c || (c.length == 1 && c.first == 4)
      loops << [x,y,c]
    end
  end

  loops
end


def find_loops_aux(solution, field, direction, x, y)
  cur_direction = [U_CON, D_CON, L_CON, R_CON]
  while !field[y][x]
    num = solution[y][x]
    if !num then break end
    # puts "** find_loops_aux at #{y},#{x} num = #{num}"
    # puts "** find_loops_aux at #{y},#{x} direction = #{direction} cur_direction = #{cur_direction[direction]}"
    num = num - cur_direction[direction]
    direction = num.first
    # puts "** find_loops_aux at #{y},#{x} remain = #{num.first}"
    solution[y][x] = nil
    case num.first
    when 0 then if solution[y - 1][x] then y -= 1 end
    when 1 then if solution[y + 1][x] then y += 1 end
    when 2 then if solution[y][x - 1] then x -= 1 end
    when 3 then if solution[y][x + 1] then x += 1 end   
    else break
    end
  end
end


def add_loop_constraint(solver, vars, solution, loops)  
  base_vars = []
  solution.each_with_index do |line, y|
    line.each_with_index do |c, x|

    end
  end

  loops.each do |ary|
    x = ary[0]
    y = ary[1]
    new_vars = []
    ary[2].each do |pattern_index|
      # puts "add loops constrain #{y},#{x} #{pattern_index} #{vars[y][x].first.length}"
      new_vars << -vars[y][x].first[pattern_index]
    end  
    solver << new_vars    
  end
end

def solve(solver, vars, field, prog_msg, found_msg, not_found_msg)
  trial = 0
  loop do
    trial += 1
    puts "#{ prog_msg }... (trial #{ trial })"
    puts "clauses : #{ solver.clause_size }"

    start = Time.now
    result = solver.solve
    eplise = Time.now - start
    running_time = "Running time: %.6f sec." % eplise    
    unless result
      puts not_found_msg
      return false
    end

    puts "translating model into solution..."
    solution = make_solution(solver, vars, field)

    loops = find_loops(solution, field)
    # puts "*** loops = #{loops}"
    add_loop_constraint(solver, vars, solution, loops)

    if loops.empty?
      puts found_msg
      puts running_time
      stack(width: 1000, left: 50, top: 70) { para running_time } 
      output_field(solution, field)
      puts
      return true
    end
  end
end

def start_program(file)
  field = parse_file(file) 

  solver = MiniSat::Solver.new

  puts "defining SAT..."
  vars = define_sat(solver, field)
  puts "variables : #{ solver.var_size }"
  puts

  str = $filled ? "(filled) solution" : "solution"

  prog_msg = "solving SAT"
  found_msg = "#{ str } found."
  not_found_msg = "unsolvable."
  solve(solver, vars, field, prog_msg, found_msg, not_found_msg) or exit 1

  # add_constraint(solver, vars)

  # prog_msg = "finding different #{ str }"
  # found_msg = "different #{ str } found."
  # not_found_msg = "different #{ str } not found."
  # solve(solver, vars, field, prog_msg, found_msg, not_found_msg) and exit 1
  # puts
  # puts
end

Shoes.app width: 1200, height: 800 do
   stack left:50 do
     # Background, text and a button: both are elements!
     @back  = background green
     @input  = banner "Input file: "
     @solve_btn = button "Start"
     # And so, both can be styled.     
     @input.style size: 12, stroke: red, margin: 10
     @solve_btn.style width: 200
     @back.style height: 10

     @solve_btn.click{
        filename = ask_open_file
        @input.replace filename  
        start_program(filename)
     }
   end
 end