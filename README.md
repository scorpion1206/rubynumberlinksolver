## Prerequisite:

- Install ruby-dev: gem install ruby-dev
- Install ruby-minisat: _gem install ruby-minisat_  
- For numberlink_solver_with_GUI, install Shoes for Ruby : http://shoesrb.com/  

## To run solver in terminal:

- sudo chmod +x numberlink_solver.rb
- ./numberlink_solver.rb sample_inputs/input1.txt  
or ./numberlink_solver.rb -b sample_inputs/input1.txt  
or ./numberlink_solver.rb -f sample_inputs/input1.txt  
or ./numberlink_solver.rb -uc sample_inputs/input1.txt  
or ./numberlink_solver.rb -ucf sample_inputs/input1.txt  

## To run GUI solver:

- Replace the "#! /home/gemini/.shoes/federales/shoes" in numberlink_solver_with_GUI.rb by your path to shoes program  
- _sudo chmod +x numberlink_solver_with_GUI.rb_  
- _./numberlink_solver_with_GUI.rb_
